##Augmentation mechanics:

Everyone gets 3 Augmentation Points during character creation. These points buy advantages for the Augmentation Roll. You do one Augmentation Roll (1d100) per augmentation/prosthetic your character has, to determine its quality. First you spend the points, then you do the rolls. Multiple points can be spent on a single roll. Unspent points are banked for the next time you obtain an augmentation.

During character creation you get up to 2 major augmentations. 

1 Augmentation Point buys:

- +15 on a specific Augmentation Roll
- 1 possible reroll (is spent whether or not you choose to actually reroll)
- choose dice breakdown for one specific Augmentation Roll (e.g. 5d20 instead of 1d100)
- 1 extra, safe, minor, beneficial augmentation chosen by GM

2 augmentation points buy:

- *Limited Warranty*: you know a mechanic who gives you a 40% discount on repairs to this one augmentation. Can work as repairman for entire team, on conventional hardware. Discount does not apply to upgrades or other augmentations
- *Could've been worse*: reroll this Augmentation Roll until you exceed 40
