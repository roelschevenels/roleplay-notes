## Augmentation Table

*If you're rolling for one of your first two augmentations, you may choose to use the legacy augmentation table instead.*

- **01-05**: *Broken*, completely nonfunctional, in case of a prosthetic this means you're handicapped
- **06-20**: *Damaged*, unreliable, has outages or fails randomly (but not handicapped)
- **21-40**: *Flimsy*, frail, keep away from water, needs near-daily maintenance to continue working
- **41-60**: *Decent*, good working condition, splash proof, occasional tune-up maintenance keeps it in good shape but is not strictly necessary, solid replacement for human limb
- **61-80**: *Quality Work*, built to last, comes with a repair manual and tools (should it ever get damaged), matches peak human performance (if limb)
- **81-95**: *Military-grade*, environment-adaptable, has independent failure of modules, easily exceeds human performance, 100% waterproof
- **96-100**: *Damn son*, where'd you find this? **[natural-only]** (Mystery! You get a rare augmentation that just might have a near-unique feature)

**Natural-only** means that modifiers (e.g. +15) don't count towards achieving this tier. If you land on this tier after applying modifiers (but not before), you are downgraded to the first noncritical tier below it OR the tier you would achieve with a natural roll (your choice).
