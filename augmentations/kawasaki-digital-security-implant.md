## Kawasaki standard-issue neural implant

C-tier augmentation

Neural implant that connects to the Kawasaki adapter, allows wireless interfaceless communication over whichever port the adapter is connected to.

*Given to certain employees of Messerschmitt-Kawasaki as a reward for their loyalty, and as a tool to fulfill their duties.*

## Kawasaki standard-issue adapter

C-tier hardware

Changes shape to fit in a large variety of ports, busses, and other connectors.

*It would be too impractical to carry a connector for every port that declares itself a universal standard. Instead you carry an adapter that can shapeshift on one end, but connect to only a single device on the other.*

## Barebones bruteforcer

D-tier malware

Simple bruteforcing script, can be deployed through the Kawasaki adapter. The difficulty of the roll depends on the target device.

*When in doubt, try the shotgun approach.*

## Forkbomb

D-tier malware

Simple script that forks itself until all memory is consumed. The device may then either shut down or restart. Can be deployed over the Kawasaki adapter. The difficulty of the roll depends on the target device.

*:(){ :|:& };:*
