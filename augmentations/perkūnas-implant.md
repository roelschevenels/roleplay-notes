## Perkūnas Forearm Implant

A rare forearm implant that channels bodily energy into electrical energy. Markings indicate that this is the *Perkūnas* model.

*Developed by COLE, it is an experiment in unconventional augmentations, designed to be lethal yet covert. It was implanted into unwilling recipients, since no one who worked on the project dared brave the risk of the very painful and unpredictable backfires, where the user instead of the target received the high-voltage shock. Even when it works as intended, the process of harvesting energy from the body is painful enough that only the mad or the desperate would use it.*

B-tier augmentation:

Allows the user to cast Smite every other Full Action. If a user has two implants, they may choose to alternate them and essentially double their rate of fire, to once every Full Action.

Instead of *Psychic Phenomena*, on a roll with two matching digits the augmentation misfires: It strikes a random target in addition to the intended target. This random target can be anyone in range, including the user.

If the augmentation is *damaged*:

- A -15 modifier on Willpower for the rolls of this augmentation
- A roll of 91 or greater strikes the user and not the intended target.

If the augmentation is *flimsy*:

- A -5 modifier on Willpower for the rolls of this augmentation
- A roll of 97 or greater strikes the user and not the intended target.