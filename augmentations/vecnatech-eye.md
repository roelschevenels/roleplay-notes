## VecnaTech Eye

An eye prosthetic of unassuming design, marked with nothing but the words *VecnaTech Ocular Prosthetic Prototype Mark I*.

*Scientists have often said that the eye is the most complex human organ after the brain. It is curious then, that unlike the augmentations found in stores this unassuming electronic replacement can parse the information from its millions of photoreceptors quickly and precisely enough that the user experiences no discomfort. It does not sacrifice the usual additions to an eye prosthetic for this: photography and video recording. Much less usual is the support for low-light vision and seeing heat signatures, as well as having a considerable level of zoom. The most impressive part, however, is the fact that it appears to slowly repair itself when damaged. Whether this is accomplished through nanotechnology or something else is unclear.*

*There is no publicly traded company, no trademark, no usual paper trail indicating that VecnaTech exists. Perhaps the reason for that is that many people would be willing to steal whatever technology allows them to get this far ahead of any competition.*

S-tier Augmentation:

- Photography and video recording
- Low-light vision mode
- Heat vision mode
- Up to 10x magnification

After sustaining light to medium damage, this augmentation will repair itself over the next 24 hours.

This augmentation grants the wearer the following abilities:

**Aim for the heart**: When using the Aim skill in combat, the bonus is increased: Using Aim as a Half Action grants a +20 bonus to the character's Weapon Skill or Ballistic Skill for the next attack. Using Aim as a Full action grants a +30 bonus to the next attack.

**Eagle-eyed**: Provided the character has no negative status affecting the steadiness of their arm, the range of any gun they use gets a bonus of 50% of the base range.