## Weathered mechanical arm

C-tier prosthetic limb

*This mechanical arm was mass-manufactured by the now-bankrupt Hodges & Wong corporation. Scuffed and scratched, it looks like it has been repeatedly damaged and repaired with parts from a variety of sources, applied by a skilled hand. More care has been put into its repeated repair than in its manufacturing. Small crevices near the joints indicate that you had better not go swimming with it, unless you find some decent synthskin to cover it.*

Punches for 1d6+2 impact damage.