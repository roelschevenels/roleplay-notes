## 4D Character sheet

### Background

Taken as an infant from a middle class outerworld family by a covert government branch. Educated in electromancy and charged to conduct unspeakable experiments which rendered this brilliant mind mad, never to see the light of day. That is, until the escape. The government lab was left to burn along with the abductors, now the world has opened up as fucked up playground, waiting to be the next experiment.

### Augmentations

- Left arm:
	- Perkūnas implant *[flimsy]* in forearm
- Right arm:
	- Perkūnas implant *[quality work]* in forearm

### Character Advancement

XP to spend: 450

#### Mechanical background

- Homeworld: Voidborn
	- Fate: 3
	- Aptitude: Intelligence
	- *Child of the Dark*: Start with the *Strong Minded* talent, gain a +30 bonus to tests for moving in zero gravity
- Background: Adeptus Arbites (translated)
	- Skills: Awareness, Common Lore (Science, Underworld), Interrogation, Intimidate, Scrutiny
	- Talents: Weapon Training: Shock
	- Aptitude: Offence
	- *The Face of Madness*: (*The Face of the Law*) You can re-roll any Intimidation or Interrogation test, and substitute your Willpower bonus for degrees of success on these tests
- Role: Warrior
	- Talent: *Iron Jaw*: Test Toughness to overcome Stunning
	- Aptitudes: Ballistic Skill, Defence, Offence, Strength, Weapon Skill
	- *Expert at Violence*: In addition to the normal uses of Fate points (see page 293), after making a successful attack test, but before determining hits, a Warrior character may spend a Fate point to substitute his Weapon Skill (for melee) or Ballistic Skill (for ranged) bonus for the degrees of success scored on the attack test.

#### Purchased Skills

- Dodge
- Parry
- Stealth

#### Purchased Talents & Traits

- Disarm
- Takedown
- Counterattack

### Stats

12 wounds

```
WS  45   INT  30
BS  25   PER  25
S   20   WP   45
T   30   FEL  25
AG  45   IFL  25
```

### Equipment

- Ceramic blade (sword) [Melee, Dmg: 1d10 R, Pen: 0 Special: Balanced]
- Katana (sword) [Melee, Dmg: 1d10 R, Pen: 0 Special: Balanced]
- Essential augmentation repair tools
- Drug capsule injector
- £4540
- 5 capsules of Neuropozyne

### Fate Points

Threshold: 3
Current: 0
