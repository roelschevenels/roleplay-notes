## Egbert Draumar Character sheet

### Background

Being born in a family of middle-managers at Ruhr Datafax, a Saeder-Krupp subsidiary, his youth was spent in the relative safety of the corporate housing blocks. With the easy access to education and employment that comes from this situation, he guaranteed himself a position as a personal assistant to Sam Weitner, head of security at Messerschmitt-Kawasaki. In this function he recieved additional training in martial arms, security protocols and tactics, and corporate diplomacy.

However, after a devastating sabotage strike on the M-K headquarters, he found himself laying low, hiding from both corporate competitors and internal liquidation. So far he has managed by relying on his contacts up the grapevine and in the shadows. Though everybodys luck runs dry someday.

Though his attire no longer bears the marks of his employers, it is clearly corporate issue of decent stock. Egbert wears an anonimised navy-colored uniform with reinforced inlays and damaged/removed silver trimmings. Whenever possible he hides his dark blonde hair and green eyes underneath his helmet and visor. If one were to see underneath his daily attire, once could see clean, yet prominent implants and surgical scars. These are mainly a preparatory framework for future enhancements that, at this point, may never come. Even so, he makes full use of the limited communications qnd security suite already present in the framework.

His manner is softspoken and polite, trying to blend into the background, ready to intervene only when required or asked.


### Augmentations

- Neural:
	- Kawasaki standard-issue neural implant

### Character Advancement

XP to spend: 750

#### Mechanical background

- Homeworld: Highborn
	- Fate: 4
	- Aptitude: Fellowship
	- *Breeding Counts*: Anything that would reduce Influence reduces it by 1 less (minimum reduction 1)
- Background: Outcast
	- Skills: Sleight of Hand, Common Lore (Underworld), Deceive, Dodge, Stealth
	- Talents: Weapon Training: Chain, Pistols
	- Aptitude: Social
	- *Never Quit*: When determining Fatigue, count +2 to your toughness bonus
- Role: Seeker
	- Talent: *Keen Intuition*: Reroll awareness with -10 penalty on first failure
	- Aptitudes: Fellowship, Intelligence, Perception, Social, Tech
	- *Nothing Escapes My Sight*: Spend a Fate point to automatically succeed at Awareness/Inquiry skill test. Degrees of success equals the Perception bonus.

#### Purchased Skills

- Awareness
- Charm
- Command
- Inquiry
- Medicae
- Scrutiny
- Security
- Tech use

### Stats

6/10 wounds (Lightly damaged)

```
WS  25   INT  40
BS  30   PER  25
S   25   WP   25
T   25   FEL  45
AG  40   IFL  30
```

### Equipment

- Armoured Bodyglove
- 2 capsules of Slaught
- Bag of shiny powder
- Drug capsule injector
- Autopistol
- Heavy Duty EMP grenade [Thrown, Dmg: 2d10 X, Pen: 0 Special: Reboots augmentations]
- Telescoping baton (truncheon)
- Kawasaki standard-issue adapter
	- Barebones Bruteforcer
	- Forkbomb
- £3640
- 5 capsules of Neuropozyne

### Fate Points

Threshold: 3
Current: 1
