## Nova Character sheet

### Background

Raised an orphan in junkjard city where she sorted junk for food. In her teens she got into the gang work; bribing, intimidations, stealing, killing, ... She was quite the big shot and earned a lot of respect on the streets. Now she's trying to make her way up to higher social tiers by any means necesary.

### Augmentations

- Right eye:
	- VecnaTech Eye *[S-tier]* as prosthetic
- Right arm:
	- Weathered mechanical arm *[decent]* as prosthetic

### Character Advancement

XP to spend: 450

#### Mechanical background

- Homeworld: Hive World
	- Fate: 3
	- Aptitude: Perception
	- *Teeming Masses in Metal Mountains*: For movement purposes, crowds count as open ground. When in enclosed spaces, gain +10 to Navigate(Surface)
- Background: Outcast
	- Skills: Acrobatics, Common Lore (Underworld), Deceive, Dodge, Stealth
	- Talents: Weapon Training: Rifles
	- Aptitude: Offence
	- *Never Quit*: When determining Fatigue, count +2 to your toughness bonus
- Role: Warrior
	- Talent: *Quick Draw*: For pistols and basic ballistic weapons, *Ready* counts as a Free Action
	- Aptitudes: Agility, Ballistic Skill, Defence, Fellowship, Finesse
	- *Move and Shoot*: once per round, one standard attack with wielded pistol as free action

#### Purchased Skills

- Awareness
- Charm
- Inquiry
- Scrutiny
- Navigate (Surface)

#### Purchased Talents & Traits

- *Hip Shooting*: You may make a Standard Attack after a full move
- *Clues from the Crowd*: After failing an inquiry, you may reroll with -10

### Stats

10 wounds

```
WS  25   INT  30
BS  45   PER  45
S   25   WP   40
T   25   FEL  35
AG  30   IFL  25
```

### Equipment

- Flak Vest
- Antique Revolver [Unreliable]
	- Pistol 30m S/2/- 1d10+2 I  0  6  1F
- Hunting Rifle
	- Rng 150m
	- RoF     S/-/-
	- Dmg     1d10+3 I
	- Pen     0
	- Clip    2
	- Reload  2 Full
- 2 capsules of Slaught
- Drug capsule injector
- Stolen laptop
- £1200
- 5 capsules of Neuropozyne

### Fate Points

Threshold: 3
Current: 1
