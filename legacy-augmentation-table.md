## Legacy Augmentation Table

*This table may only be used for your first two augmentations.*

- **01-10**: *Broken*, completely nonfunctional, in case of a prosthetic this means you're handicapped
- **11-25**: *Damaged*, unreliable, has outages or fails randomly (but not handicapped)
- **26-40**: *Flimsy*, frail, keep away from water, needs near-daily maintenance to continue working
- **41-60**: *Decent*, good working condition, splash proof, occasional tune-up maintenance keeps it in good shape but is not strictly necessary, solid replacement for human limb
- **61-75**: *Quality Work*, built to last, comes with a repair manual and tools (should it ever get damaged), matches peak human performance (if limb)
- **76-90**: *Military-grade*, environment-adaptable, has independent failure of modules, easily exceeds human performance, 100% waterproof
- **91-100**: *Damn son*, where'd you find this? (mystery!)

