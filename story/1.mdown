The music is uncomfortably loud, loud enough that speaking to someone at the same table is difficult. The people who bother speaking move their mouths closer to the recipients' ear. This means that conversation is almost impossible to overhear. Perhaps that's the reason for the loud music in such a calm coffee bar. Convenient for those with something to hide, less convenient for those hoping to overhear such a secret and maybe worm their ways into a job. Anything that pays.

When overlooking the bar, one would expect most attendants to fall in the latter category. Vacant stares at the wall, before another sip of soykaf — or something worse — gives them enough of a boost to start looking around the bar again, studying people, seeing who might have money and be willing to bet it on a bunch of washed-up losers. Bet it on them pulling off some low-key theft or information gathering, something worth much more to the person paying for the job.

There's only one person in the bar who might fit that bill, really. If you didn't manage to spot him by his fancy clothes — not *too* fancy, it looks inconspicuous but the material's good — you would have spotted him because everyone else as desperate as you keeps staring at him. The gentleman, however, seems content to take his time, mostly focused on drinking his Fringe Weaver.

He glances at you. Then he looks away. You sigh, and lightly tug at your left sleeve so you can see your watch. Or perhaps you fish a device out of your pocket that tells you the time. Maybe you don't need to physically move at all for the HUD implanted in your eyeball to inform you how late it is. That's not for me to tell. It's late enough that most people have left. You wonder how much longer you're going to waste before the gentleman starts talking to someone (you don't bother hoping it's you). He glances at you again. Then he drops dead.

His forehead smashes into the tall cocktail glass, spraying shards and expensive alcohol everywhere. His body slips off the table and hits the floor.

The remaining attendants the bar has, few as they are, look at each other. Who are they, I wonder?
